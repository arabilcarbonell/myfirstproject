import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeparmentListComponent } from './deparment-list/deparment-list.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { DasboardNavComponent } from './dasboard-nav/dasboard-nav.component';


const routes: Routes = [
  {path:'department', component:DeparmentListComponent},
  {path:'home', component:MainNavComponent},
  {path:'dashboard', component:DasboardNavComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  DeparmentListComponent,MainNavComponent,DasboardNavComponent
]